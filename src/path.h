/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef PATH_H
#define PATH_H


char* path_get_filename_without_extension(char* path);
char* path_get_extension(char* path);
char* path_get_cwd(void);

#endif /* PATH_H */