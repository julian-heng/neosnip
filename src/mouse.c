/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdbool.h>

#include <windows.h>

#include "mouse.h"
#include "screen.h"


static LRESULT CALLBACK _mouse_window_proc(HWND hwnd, UINT msg, WPARAM wparam,
                                         LPARAM lparam);


static POINT* g_point = NULL;
static bool g_point_set = FALSE;


void
mouse_get_coords_on_click(POINT* point)
{
    g_point = point;
    g_point_set = FALSE;

    MSG msg;
    RECT rect;

    screen_get_screen_boundaries(&rect);

    HINSTANCE h_instance = GetModuleHandle(NULL);
    const char* window_name = "mouse";

    WNDCLASSEX wc = {
        .cbSize = sizeof(WNDCLASSEX),
        .style = 0,
        .lpfnWndProc = _mouse_window_proc,
        .cbClsExtra = 0,
        .cbWndExtra = 0,
        .hInstance = h_instance,
        .hIcon = NULL,
        .hCursor = LoadCursor(NULL, IDC_CROSS),
        .hbrBackground = NULL,
        .lpszMenuName = NULL,
        .lpszClassName = window_name,
        .hIconSm = NULL,
    };

    if (RegisterClassEx(&wc) == 0)
    {
        goto cleanup_globals;
    }

    HWND hwnd = CreateWindowEx(
        WS_EX_TOOLWINDOW | WS_EX_TOPMOST | WS_EX_TRANSPARENT, window_name, NULL,
        WS_POPUP, 0, 0, 0, 0, NULL, NULL, h_instance, NULL);

    if (hwnd == NULL)
    {
        goto cleanup_globals;
    }

    ShowWindow(hwnd, SW_SHOWNA);
    MoveWindow(hwnd, rect.left, rect.top, rect.right, rect.bottom, FALSE);

    while (!g_point_set)
    {
        while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                goto cleanup;
            }

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

cleanup:
    DestroyWindow(hwnd);

cleanup_globals:
    g_point = NULL;
    g_point_set = FALSE;
}


static LRESULT CALLBACK
_mouse_window_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    if (msg == WM_LBUTTONDOWN)
    {
        GetCursorPos(g_point);
        g_point_set = TRUE;
    }

    return DefWindowProc(hwnd, msg, wparam, lparam);
}