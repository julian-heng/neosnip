/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef TARGETVER_H
#define TARGETVER_H

#include <winsdkver.h>


#define WINVER _WIN32_WINNT_VISTA
#define _WIN32_WINNT WINVER
#define NTDDI_VERSION NTDDI_VISTA


#include <sdkddkver.h>

#endif /* TARGETVER_H */