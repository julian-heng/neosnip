/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdlib.h>

#include <windows.h>
#include <psapi.h>
#include <log.c/src/log.h>

#include "mouse.h"
#include "window.h"


window_t*
window_make_from_hwnd(HWND hwnd)
{
    if (hwnd == 0)
    {
        return NULL;
    }

    window_t* window = calloc(1, sizeof(window_t));
    window->handle = hwnd;

    DWORD proc_id = 0;
    GetWindowThreadProcessId(hwnd, &proc_id);
    HANDLE h_proc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                FALSE, proc_id);
    GetModuleFileNameEx(h_proc, NULL, window->exe_path, MAX_PATH);
    CloseHandle(h_proc);

    window->title_length = GetWindowTextLength(hwnd) + 1;
    window->title = calloc(window->title_length, sizeof(char));
    GetWindowText(hwnd, window->title, window->title_length);

    GetWindowRect(hwnd, &(window->rect));

    return window;
}


window_t*
window_make_from_point(POINT pt)
{
    // Use GetAncestor to get the handle to the window and not a child of the
    // window
    HWND hwnd = WindowFromPoint(pt);
    hwnd = GetAncestor(hwnd, GA_ROOTOWNER);
    window_t* window = window_make_from_hwnd(hwnd);
    return window;
}


void
window_free(window_t** window)
{
    if (*window != NULL)
    {
        if ((*window)->title != NULL)
        {
            free((*window)->title);
            (*window)->title = NULL;
        }

        free(*window);
        *window = NULL;
    }
}


void
window_bring_to_top(window_t* window)
{
    if (window != NULL)
    {
        log_debug("bringing window '%p' to top", window->handle);
        SetForegroundWindow(window->handle);
    }
}


void
window_update(window_t* window)
{
    if (window != NULL)
    {
        log_debug("updating window '%p'", window->handle);
        UpdateWindow(window->handle);
    }
}


void
window_log(window_t* window)
{
    log_debug("window: %p", window);
    if (window == NULL)
    {
        return;
    }
    log_debug("    handle: %p", window->handle);
    log_debug("    exe_path: '%s'", window->exe_path);
    log_debug("    title: '%s'", window->title);
    log_debug("    title_length: %lu", window->title_length);
    log_debug("    rect: (%d, %d, %d, %d)",
        window->rect.left,window->rect.top,
        window->rect.right, window->rect.bottom);
}