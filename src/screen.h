/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef SCREEN_H
#define SCREEN_H

#include <stdbool.h>

#include <windef.h>


void screen_get_screen_boundaries(LPRECT rect);
void screen_get_screen_rect_from_point(POINT pt, LPRECT rect);
bool screen_rect_is_within_screen_bounds(LPRECT rect);
void screen_trim_rect_to_screen(LPRECT rect);

#endif /* SCREEN_H */