/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdint.h>

#include "screenshot.h"


typedef struct options_t
{
    char* output;
    bool prompt;
    bool clipboard;
    uint32_t background;
    bool background_flag;
    bool preview;
    bool verbose;
    screenshot_spec_t screenshot;
} options_t;


void options_init(options_t* options);
bool options_parse_command_line_arguments(int argc, char** argv,
                                          options_t* options);
void options_print_help(void);
void options_print_version(void);

#endif /* OPTIONS_H */