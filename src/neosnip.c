/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdio.h>
#include <time.h>

#include <windows.h>
#include <winuser.h>
#include <shellapi.h>

#include <log.c/src/log.h>

#include "bitmap.h"
#include "filepicker.h"
#include "image.h"
#include "mouse.h"
#include "options.h"
#include "path.h"
#include "screen.h"
#include "screenshot.h"
#include "version.h"
#include "window.h"


static void neosnip_handle_full(_screenshot_spec_t* spec);
static void neosnip_handle_selecting_screen(_screenshot_spec_t* spec);
static void neosnip_handle_selecting_window(_screenshot_spec_t* spec);
static void neosnip_save_to_clipboard(img_t* bitmap, options_t* options);
static void neosnip_save_to_file(img_t* bitmap, options_t* options);
static char* neosnip_prompt_save_dialog(void);
static char* neosnip_generate_filename_from_window(window_t* window);


int
main(int argc, char** argv)
{
    log_set_quiet(true);

    options_t options;
    options_init(&options);

    if (!options_parse_command_line_arguments(argc, argv, &options))
    {
        goto exit;
    }
    
    if (options.verbose)
    {
        log_set_quiet(false);
    }

    log_debug("%s %s", NEOSNIP_PROG, NEOSNIP_VERSION);

    img_t* bitmap = NULL;

    switch (options.screenshot.type)
    {
    case SCREENSHOT_FULL:
        log_debug("screenshot type: SCREENSHOT_FULL");
        neosnip_handle_full(&(options.screenshot.spec));
        break;

    case SCREENSHOT_SCREEN:
        log_debug("screenshot type: SCREENSHOT_SCREEN");
        neosnip_handle_selecting_screen(&(options.screenshot.spec));
        break;

    case SCREENSHOT_WINDOW:
        log_debug("screenshot type: SCREENSHOT_WINDOW");
        neosnip_handle_selecting_window(&(options.screenshot.spec));
        break;

    case SCREENSHOT_UNKNOWN:
    default:
        log_debug("screenshot type: SCREENSHOT_UNKNOWN");
        break;
    }

    bitmap = screenshot_run(&(options.screenshot));
    if (bitmap == NULL)
    {
        log_error("failed to run screenshot task");
        goto exit_cleanup;
    }

    if (options.clipboard)
    {
        neosnip_save_to_clipboard(bitmap, &options);
    }
    else
    {
        neosnip_save_to_file(bitmap, &options);

        if (options.preview && options.output != NULL)
        {
            log_debug("opening screenshot '%s'", options.output);
            ShellExecuteA(NULL, "open", options.output, NULL, NULL, SW_SHOW);
        }
    }

exit_cleanup:
    image_free(&bitmap);

    if (options.screenshot.type == SCREENSHOT_WINDOW)
    {
        window_free(&(options.screenshot.spec.window.window));
    }

exit:
    return 0;
}


static void
neosnip_handle_full(_screenshot_spec_t* spec)
{
    screen_get_screen_boundaries(&(spec->capture));
}


static void
neosnip_handle_selecting_screen(_screenshot_spec_t* spec)
{
    log_debug("selecting screen...");
    POINT pt;
    mouse_get_coords_on_click(&pt);
    screen_get_screen_rect_from_point(pt, &(spec->capture));
}


static void
neosnip_handle_selecting_window(_screenshot_spec_t* spec)
{
    log_debug("selecting window...");
    POINT pt;
    mouse_get_coords_on_click(&pt);
    spec->window.window = window_make_from_point(pt);
}


static void
neosnip_save_to_clipboard(img_t* bitmap, options_t* options)
{
    log_debug("saving to clipboard...");
    HBITMAP copy;

    if (!OpenClipboard(NULL) || !EmptyClipboard())
    {
        log_error("unable to save image to clipboard");
        return;
    }

    screenshot_spec_t screenshot = options->screenshot;
    if (screenshot.type == SCREENSHOT_WINDOW &&
        screenshot.spec.window.transparent)
    {
        // Windows removes alpha information in the clipboard, rendering a black
        // background. We would blend it against a white background or a user
        // set background colour instead.
        image_alpha_blend(bitmap, options->background);
    }

    copy = bitmap_img_to_hbitmap(bitmap);

    SetClipboardData(CF_BITMAP, copy);
    DeleteObject(copy);
    CloseClipboard();
}


static void
neosnip_save_to_file(img_t* bitmap, options_t* options)
{
    log_debug("saving to file...");
    char* output = options->output;

    window_t* window = NULL;
    if (options->screenshot.type == SCREENSHOT_WINDOW)
    {
        window = options->screenshot.spec.window.window;
    }

    if (output == NULL)
    {
        if (options->prompt)
        {
            output = neosnip_prompt_save_dialog();
        }
        else
        {
            output = neosnip_generate_filename_from_window(window);
        }

        // TODO: Validate output filename
        if (output == NULL)
        {
            // TODO: Better way to show that saving to file failed
            log_debug("failed to set filename");
            return;
        }

        options->output = output;
    }

    // Apply alpha blend if background is given and is a transparent screenshot
    // of a window
    screenshot_spec_t screenshot = options->screenshot;
    if (screenshot.type == SCREENSHOT_WINDOW &&
        screenshot.spec.window.transparent &&
        options->background_flag)
    {
        image_alpha_blend(bitmap, options->background);
    }

    log_debug("saved file to '%s'", output);
    image_write(bitmap, output, IMAGE_UNKNOWN);
}


static char*
neosnip_prompt_save_dialog(void)
{
    log_debug("opening save dialog...");
    char* filter = filepicker_make_filter((filter_t[]){
        {"PNG Image File (*.png)", "*.png"},
        {"JPG Image File (*.jpg, *.jpeg)", "*.jpg;*.jpeg;"},
        {"BMP Image File (*.bmp)", "*.bmp"},
        {"TGA Image File (*.tga)", "*.tga"},
        {NULL, NULL},
    });
    char* cwd = path_get_cwd();

    return filepicker_open_save_dialog(cwd, filter);
}


static char*
neosnip_generate_filename_from_window(window_t* window)
{
    static char buf[MAX_PATH];
    int timestamp = (int)time(NULL);

    // TODO: Support saving to different image formats
    if (window != NULL)
    {
        snprintf(buf, MAX_PATH, "%s_%d.png",
                 path_get_filename_without_extension(window->exe_path),
                 timestamp);
    }
    else
    {
        snprintf(buf, MAX_PATH, "%d.png", timestamp);
    }

    return buf;
}