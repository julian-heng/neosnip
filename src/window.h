/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef WINDOW_H
#define WINDOW_H

#include <windows.h>


typedef struct window_t
{
    HWND handle;
    char exe_path[MAX_PATH];
    char* title;
    size_t title_length;
    RECT rect;
} window_t;


window_t* window_make_from_hwnd(HWND hwnd);
window_t* window_make_from_point(POINT pt);
void window_free(window_t**);

void window_bring_to_top(window_t* window);
void window_update(window_t* window);
void window_log(window_t* window);

#endif /* WINDOW_H */