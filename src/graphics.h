/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <windows.h>


void graphics_set_brush(HBRUSH brush);
HWND graphics_make_rectangle(RECT bounds);
void graphics_refresh(HWND hwnd);
void graphics_close_rectangle(HWND hwnd);

#endif /* GRAPHICS_H */