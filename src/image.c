/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

#include "path.h"

#include "image.h"


img_t*
image_new(uint32_t width, uint32_t height)
{
    img_t* img;
    size_t size;

    img = calloc(1, sizeof(img_t));
    if (img == NULL)
    {
        return NULL;
    }

    size = width * height;
    img->width = width;
    img->height = height;

    img->data = malloc(sizeof(uint32_t) * size);
    if (img->data == NULL)
    {
        free(img);
        return NULL;
    }

    memset(img->data, 0xFFFFFFFF, sizeof(uint32_t) * size);

    return img;
}


img_t*
image_new_from_img(img_t* _img)
{
    return image_new(_img->width, _img->height);
}


void
image_free(img_t** img)
{
    if (*img != NULL)
    {
        if ((*img)->data != NULL)
        {
            free((*img)->data);
            (*img)->data = NULL;
        }

        free(*img);
        *img = NULL;
    }
}



void
image_blank(img_t* img)
{
    size_t size;
    size = img->width * img->height;
    memset(img->data, 0xFFFFFFFF, sizeof(uint32_t) * size);
}


void
image_write(img_t* img, char* path, img_ext_t ext)
{
    if (ext == IMAGE_UNKNOWN)
    {
        ext = image_str_to_ext(path_get_extension(path));
    }

    switch (ext)
    {
    case IMAGE_PNG:
        image_write_png(img, path);
        break;

    case IMAGE_BMP:
        image_write_bmp(img, path);
        break;

    case IMAGE_TGA:
        image_write_tga(img, path);
        break;

    case IMAGE_JPG:
        image_write_jpg(img, path);
        break;

    case IMAGE_UNKNOWN:
        break;
    }
}


void
image_write_png(img_t* img, char* path)
{
    stbi_write_png(path,
                   img->width,
                   img->height,
                   4,
                   img->data,
                   img->width * 4);
}


void
image_write_bmp(img_t* img, char* path)
{
    stbi_write_bmp(path, img->width, img->height, 4, img->data);
}


void
image_write_tga(img_t* img, char* path)
{
    stbi_write_tga(path, img->width, img->height, 4, img->data);
}


void
image_write_jpg(img_t* img, char* path)
{
    stbi_write_jpg(path,
                   img->width,
                   img->height,
                   4,
                   img->data,
                   img->jpg_quality);
}


char*
image_ext_to_str(img_ext_t ext)
{
    switch (ext)
    {
    case IMAGE_PNG: return "png"; break;
    case IMAGE_BMP: return "bmp"; break;
    case IMAGE_TGA: return "tga"; break;
    case IMAGE_JPG: return "jpg"; break;
    case IMAGE_UNKNOWN: return ""; break;
    }

    return NULL;
}


img_ext_t
image_str_to_ext(char* extstr)
{
    if (extstr == NULL)
    {
        return IMAGE_UNKNOWN;
    }
    else if (strncasecmp(extstr, "png", 3) == 0)
    {
        return IMAGE_PNG;
    }
    else if (strncasecmp(extstr, "bmp", 3) == 0)
    {
        return IMAGE_BMP;
    }
    else if (strncasecmp(extstr, "tga", 3) == 0)
    {
        return IMAGE_TGA;   
    }
    else if (strncasecmp(extstr, "jpg", 3) == 0 ||
             strncasecmp(extstr, "jpeg", 4) == 0)
    {
        return IMAGE_JPG;
    }
    else
    {
        return IMAGE_UNKNOWN;
    }
}


bool
image_equals(img_t* a, img_t* b)
{
    if (a->width != b->width)
    {
        return false;
    }

    if (a->height != b->height)
    {
        return false;
    }

    uint32_t* i = a->data;
    uint32_t* j = b->data;

    do
    {
        if (*i != *j)
        {
            return false;
        }
    } while (*++i && *++j);

    return true;
}


bool
image_validate_path(char* path)
{
    if (path == NULL)
    {
        return false;
    }

    if (image_str_to_ext(path_get_extension(path)) == IMAGE_UNKNOWN)
    {
        return false;
    }

    return true;
}


void
image_alpha_blend_inverse(img_t* white, img_t* black, img_t* result)
{
    size_t size = white->width * white->height;
    for (size_t i = 0; i < size; i++)
    {
        uint32_t w_px = white->data[i];
        uint32_t b_px = black->data[i];
        uint32_t r_px = 0;

        double alpha = (IMAGE_R(b_px) - IMAGE_R(w_px) + 255) / 255.0;

        if (alpha == 0)
        {
            r_px = 0;
        }
        else if (0 < alpha && alpha < 1)
        {
            uint32_t a = (uint32_t)(alpha * 255);
            uint32_t r = (uint32_t)(IMAGE_R(b_px) / alpha);
            uint32_t g = (uint32_t)(IMAGE_G(b_px) / alpha);
            uint32_t b = (uint32_t)(IMAGE_B(b_px) / alpha);
            r_px = IMAGE_ARGB(a, r, g, b);
        }
        else
        {
            r_px = w_px;
        }

        result->data[i] = r_px;
    }
}


void
image_alpha_blend(img_t* img, uint32_t colour)
{
    size_t size = img->width * img->height;
    for (size_t i = 0; i < size; i++)
    {
        uint32_t pixel = img->data[i];
        double alpha = IMAGE_A(pixel) / 255.0;
        double m_alpha = 1 - alpha;

        uint32_t c_r = IMAGE_R(colour);
        uint32_t c_g = IMAGE_G(colour);
        uint32_t c_b = IMAGE_B(colour);

        uint32_t r = (IMAGE_R(pixel) * alpha) + (m_alpha * c_r);
        uint32_t g = (IMAGE_G(pixel) * alpha) + (m_alpha * c_g);
        uint32_t b = (IMAGE_B(pixel) * alpha) + (m_alpha * c_b);

        img->data[i] = IMAGE_ARGB(0xFF, r, g, b);
    }
}