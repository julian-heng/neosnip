!define APP_NAME "neosnip"

OutFile "${APP_NAME}-installer.exe"
Name "${APP_NAME}"


InstallDir "$APPDATA\${APP_NAME}\"
ShowInstDetails show
ShowUninstDetails show
SetCompressor /SOLID lzma


DirText "\
Setup will install neosnip in the following folder. To install in a different \
folder, click Browse and select another folder. Click Install to continue."


Section
    SetOutPath $INSTDIR
    File bin\neosnip.exe
    WriteUninstaller "Uninstall.exe"
SectionEnd


Section "Uninstall"
    SetOutPath $INSTDIR
    Delete "$INSTDIR\Uninstall.exe"
    Delete "$INSTDIR\neosnip.exe"
    SetOutPath $APPDATA
    RMDir "$INSTDIR"
SectionEnd