/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef FILEPICKER_H
#define FILEPICKER_H


typedef struct filter_t
{
    const char* name;
    const char* glob;
} filter_t;


char* filepicker_open_save_dialog(char* directory, char* filter);
char* filepicker_make_filter(const filter_t* filters);

#endif /* FILEPICKER_H */