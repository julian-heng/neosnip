/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2021 Julian Heng */

#include "targetver.h"

#include <stdint.h>

#include <windows.h>

#include <dwmapi.h>
#include <wingdi.h>

#include <log.c/src/log.h>

#include "graphics.h"


static LRESULT CALLBACK _graphics_win_proc(HWND hwnd, UINT msg, WPARAM wparam,
                                           LPARAM lparam);
static void _graphics_handle_create(HWND hwnd);
static void _graphics_handle_paint(HWND hwnd);


static HBRUSH g_brush = NULL;


void
graphics_set_brush(HBRUSH brush)
{
    g_brush = brush;
}


HWND
graphics_make_rectangle(RECT bounds)
{
    log_debug("graphics_make_rectangle");

    int32_t x = bounds.left;
    int32_t y = bounds.top;
    int32_t w = bounds.right - bounds.left;
    int32_t h = bounds.bottom - bounds.top;

    HINSTANCE h_instance = GetModuleHandle(NULL);

    const char* window_name = "graphics";

    WNDCLASSEX wc = {
        .cbSize = sizeof(WNDCLASSEX),
        .style = 0,
        .lpfnWndProc = _graphics_win_proc,
        .cbClsExtra = 0,
        .cbWndExtra = 0,
        .hInstance = h_instance,
        .hIcon = NULL,
        .hCursor = NULL,
        .hbrBackground = NULL,
        .lpszMenuName = 0,
        .lpszClassName = window_name,
    };

    if (RegisterClassEx(&wc) == 0)
    {
        return NULL;
    }

    HWND hwnd = CreateWindowEx(WS_EX_TOOLWINDOW, window_name, NULL,
                               WS_VISIBLE | WS_THICKFRAME | WS_POPUP,
                               x, y, w, h, NULL, NULL, h_instance, NULL);

    if (hwnd == NULL)
    {
        return NULL;
    }

    log_debug("created graphics window: %p", hwnd);

    ShowWindow(hwnd, SW_SHOWNOACTIVATE);

    LONG lstyle = GetWindowLong(hwnd, GWL_STYLE);
    lstyle |= WS_THICKFRAME;
    lstyle &= ~WS_CAPTION;
    SetWindowLong(hwnd, GWL_STYLE, lstyle);
    SetWindowPos(hwnd, NULL, 0, 0, 0, 0,
                 SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

    UpdateWindow(hwnd);

    return hwnd;
}


void
graphics_refresh(HWND hwnd)
{
    log_debug("graphics_refresh");
    log_debug("refreshing graphics window: %p", hwnd);
    RedrawWindow(hwnd, NULL, NULL,
                 RDW_INVALIDATE | RDW_ERASENOW | RDW_UPDATENOW);
}


void
graphics_close_rectangle(HWND hwnd)
{
    log_debug("graphics_close_rectangle");
    log_debug("closing graphics window: %p", hwnd);
    DestroyWindow(hwnd);
}


static LRESULT CALLBACK
_graphics_win_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{

    switch (msg)
    {
    case WM_CREATE:
        _graphics_handle_create(hwnd);
        break;

    case WM_PAINT:
        _graphics_handle_paint(hwnd);
        return 1;

    case WM_NCACTIVATE:
        return 0;

    case WM_NCCALCSIZE:
        if (lparam)
        {
            return 0;
        }
        break;

    case WM_DESTROY:
        return 0;
    }

    return DefWindowProc(hwnd, msg, wparam, lparam);
}


static void
_graphics_handle_create(HWND hwnd)
{
    static RECT border_thickness;
    SetRectEmpty(&border_thickness);

    LONG_PTR style = GetWindowLongPtr(hwnd, GWL_STYLE);

    if (style & WS_THICKFRAME)
    {
        AdjustWindowRectEx(&border_thickness, style & ~WS_CAPTION, FALSE, 0);
        border_thickness.left *= -1;
        border_thickness.top *= -1;
    }
    else if (style & WS_BORDER)
    {
        SetRect(&border_thickness, 1, 1, 1, 1);
    }

    MARGINS margins = {0};
    DwmExtendFrameIntoClientArea(hwnd, &margins);
    SetWindowPos(hwnd, NULL, 0, 0, 0, 0,
                 SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
}


static void
_graphics_handle_paint(HWND hwnd)
{
    PAINTSTRUCT ps;
    HDC hdc_screen = BeginPaint(hwnd, &ps);

    RECT rc = ps.rcPaint;
    BP_PAINTPARAMS params = {
        .cbSize = sizeof(params),
        .dwFlags = BPPF_NOCLIP | BPPF_ERASE
    };

    HDC hdc_memory;
    HPAINTBUFFER h_buffer = BeginBufferedPaint(hdc_screen, &rc, BPBF_TOPDOWNDIB,
                                               &params, &hdc_memory);

    FillRect(hdc_memory, &rc, g_brush);

    BufferedPaintSetAlpha(h_buffer, &rc, 255);
    EndBufferedPaint(h_buffer, TRUE);

    EndPaint(hwnd, &ps);
}