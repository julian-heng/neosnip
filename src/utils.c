/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <windows.h>
#include <dwmapi.h>

#include "image.h"
#include "utils.h"


bool
utils_dwm_is_composition_enabled(void)
{
    BOOL pf_enabled;
    DwmIsCompositionEnabled(&pf_enabled);
    return pf_enabled;
}


uint32_t
utils_parse_argb(const char* s)
{
    // Support long and short form argb syntax
    //
    // #FA9      => #FFFFAA99
    // #8FA9     => #88FFAA99
    // #123456   => #FF123456
    // #88123456 => #88123456
    //
    // If alpha is omitted, then it is assumed to be 0xFF
    // Any errors would result in the resulting color to be 0xFFFFFFFF

    if (s == NULL || s[0] != '#')
    {
        return 0xFFFFFFFF;
    }

    s++;

    // Validate hex string
    if (!utils_is_hexstring(s))
    {
        return 0xFFFFFFFF;
    }

    uint32_t parse = (uint32_t)strtoul(s, NULL, 16);

    uint8_t a = 0xFF;
    uint8_t r = 0xFF;
    uint8_t g = 0xFF;
    uint8_t b = 0xFF;

    switch (strlen(s))
    {
    case 4:
        a = (parse & 0x0000F000) >> 12;
        a = (a << 8) & a;
        // Fallthrough

    case 3:
        r = (parse & 0x00000F00) >> 8;
        g = (parse & 0x000000F0) >> 4;
        b = parse & 0x0000000F;

        r = (r << 4) | r;
        g = (g << 4) | g;
        b = (b << 4) | b;
        break;

    case 8:
        a = (parse & 0xFF000000) >> 24;
        // Fallthrough

    case 6:
        r = (parse & 0x00FF0000) >> 16;
        g = (parse & 0x0000FF00) >> 8;
        b = parse & 0x000000FF;
        break;
    }

    return IMAGE_ARGB(a, r, g, b);
}


bool
utils_is_hexstring(const char* s)
{
    size_t len = strlen(s);
    for (size_t i = 0; i < len; i++)
    {
        char c = s[i];
        if (!isdigit(c) && !((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')))
        {
            return false;
        }
    }

    return true;
}


bool
utils_is_windows_11()
{
    char buf[BUFSIZ];
    DWORD buf_size = sizeof(buf);

    memset(buf, '\0', sizeof(buf));
    DWORD ret = RegGetValueA(HKEY_LOCAL_MACHINE,
                             "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                             "CurrentBuildNumber", RRF_RT_REG_SZ, NULL, buf,
                             &buf_size);

    if (ret != ERROR_SUCCESS)
    {
        return false;
    }

    long build_number = strtol(buf, NULL, 0);
    return build_number >= 22000;
}