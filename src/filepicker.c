/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdio.h>
#include <string.h>

#include <windows.h>
#include <commdlg.h>

#include "filepicker.h"


char*
filepicker_open_save_dialog(char* directory, char* filter)
{
    static char path[MAX_PATH];

    if (filter == NULL)
    {
        return NULL;
    }

    memset(path, '\0', MAX_PATH);

    OPENFILENAME ofn = {
        .lStructSize = sizeof(OPENFILENAME),
        .hwndOwner = NULL,
        .hInstance = NULL,
        .lpstrFilter = filter,
        .lpstrCustomFilter = NULL,
        .nMaxCustFilter = 0,
        .lpstrFile = path,
        .nMaxFile = MAX_PATH,
        .lpstrFileTitle = NULL,
        .nMaxFileTitle = 0,
        .lpstrInitialDir = directory,
        .lpstrTitle = NULL,
        .Flags = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
        .lpstrDefExt = "png",
    };

    if (!GetSaveFileName(&ofn))
    {
        return NULL;
    }

    return path;
}


char*
filepicker_make_filter(const filter_t* filters)
{
    static char filter_str[BUFSIZ];
    memset(filter_str, '\0', BUFSIZ);

    char* offset = filter_str;
    int n_offset = 0;
    int n_offset_total = 0;

    for (const filter_t* f = filters; f->name != NULL && f->glob != NULL; f++)
    {
        // Check that the filter string does not exceed BUFSIZ
        if (n_offset_total > BUFSIZ)
        {
            return NULL;
        }

        n_offset = strlen(f->name);
        strncpy(offset, f->name, BUFSIZ - n_offset_total);
        offset += n_offset + 1;
        n_offset_total += n_offset + 1;

        n_offset = strlen(f->glob);
        strncpy(offset, f->glob, BUFSIZ - n_offset_total);
        offset += n_offset + 1;
        n_offset_total += n_offset + 1;
    }

    if (n_offset_total > BUFSIZ)
    {
        return NULL;
    }

    // Ensure that the filter string ends with "\0\0"
    filter_str[n_offset_total - 1] = '\0';
    filter_str[n_offset_total] = '\0';

    return filter_str;
}