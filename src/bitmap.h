/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef BITMAP_H
#define BITMAP_H

#include "image.h"


img_t* bitmap_hbitmap_to_img(HBITMAP h_bitmap);
HBITMAP bitmap_img_to_hbitmap(img_t* img);

#endif /* BITMAP_H */