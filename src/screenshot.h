/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <stdbool.h>

#include "image.h"
#include "window.h"


typedef enum screenshot_type_t
{
    SCREENSHOT_FULL,
    SCREENSHOT_SCREEN,
    SCREENSHOT_WINDOW,
    SCREENSHOT_UNKNOWN,
} screenshot_type_t;


typedef union
{
    struct
    {
        window_t* window;
        bool transparent;
        bool shadow;
    } window;
    RECT capture;
} _screenshot_spec_t;


typedef struct screenshot_spec_t
{
    screenshot_type_t type;
    _screenshot_spec_t spec;
} screenshot_spec_t;


void screenshot_spec_init(screenshot_spec_t* spec);
img_t* screenshot_run(const screenshot_spec_t* spec);

img_t* screenshot_capture_rect(RECT rect);
img_t* screenshot_capture_window(const _screenshot_spec_t* spec);
img_t* screenshot_capture_window_transparent(const _screenshot_spec_t* spec);

#endif /* SCREENSHOT_H */