/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdbool.h>

#include <windows.h>
#include <winuser.h>

#include <log.c/src/log.h>

#include "screen.h"


typedef struct _screen_search_callback_t
{
    LPPOINT lp_point;
    LPRECT lp_rect;
} _screen_search_callback_t;


typedef struct _screen_contains_callback_t
{
    struct
    {
        POINT pt;
        bool is_within;
    } corners[4];
} _screen_contains_callback_t;


typedef struct _screen_trim_callback_t
{
    LPRECT lp_rect;
    LPRECT lp_rect_max;
    LONG area;
} _screen_trim_callback_t;


// EnumDisplayMonitors processes
static BOOL CALLBACK _screen_search_proc(HMONITOR h_monitor, HDC hdc_monitor,
                                         LPRECT lprc_monitor, LPARAM dw_data);
static BOOL CALLBACK _screen_contains_rect_proc(HMONITOR h_monitor,
                                                HDC hdc_monitor,
                                                LPRECT lprc_monitor,
                                                LPARAM dw_data);
static BOOL CALLBACK _screen_trim_rect_to_screen_proc(HMONITOR h_monitor,
                                                      HDC hdc_monitor,
                                                      LPRECT lprc_monitor,
                                                      LPARAM dw_data);

// Private functions
static LONG _screen_get_rect_area(LPRECT rect);


static BOOL CALLBACK
_screen_search_proc(HMONITOR h_monitor, HDC hdc_monitor, LPRECT lprc_monitor,
                    LPARAM dw_data)
{
    _screen_search_callback_t* data = (_screen_search_callback_t*)dw_data;

    if (PtInRect(lprc_monitor, *(data->lp_point)))
    {
        CopyRect(data->lp_rect, lprc_monitor);
        return FALSE;
    }

    return TRUE;
}


static BOOL CALLBACK
_screen_contains_rect_proc(HMONITOR h_monitor, HDC hdc_monitor,
                           LPRECT lprc_monitor, LPARAM dw_data)
{
    _screen_contains_callback_t* data = (_screen_contains_callback_t*)dw_data;

    data->corners[0].is_within = data->corners[0].is_within ||
                                 PtInRect(lprc_monitor, data->corners[0].pt);

    data->corners[1].is_within = data->corners[1].is_within ||
                                 PtInRect(lprc_monitor, data->corners[1].pt);

    data->corners[2].is_within = data->corners[2].is_within ||
                                 PtInRect(lprc_monitor, data->corners[2].pt);

    data->corners[3].is_within = data->corners[3].is_within ||
                                 PtInRect(lprc_monitor, data->corners[3].pt);

    return TRUE;
}


static BOOL CALLBACK
_screen_trim_rect_to_screen_proc(HMONITOR h_monitor, HDC hdc_monitor,
                                 LPRECT lprc_monitor, LPARAM dw_data)
{
    _screen_trim_callback_t* data =(_screen_trim_callback_t*)dw_data;

    RECT intersect;
    SetRectEmpty(&intersect);

    IntersectRect(&intersect, lprc_monitor, data->lp_rect);
    LONG intersect_area = _screen_get_rect_area(&intersect);

    if (intersect_area > data->area)
    {
        CopyRect(data->lp_rect_max, &intersect);
        data->area = intersect_area;
    }

    return TRUE;
}


static LONG
_screen_get_rect_area(LPRECT r)
{
    return (r->right - r->left) * (r->bottom - r->top);
}


void
screen_get_screen_boundaries(LPRECT rect)
{
    SetRectEmpty(rect);
    rect->left = GetSystemMetrics(SM_XVIRTUALSCREEN);
    rect->top = GetSystemMetrics(SM_YVIRTUALSCREEN);
    rect->right = GetSystemMetrics(SM_CXVIRTUALSCREEN);
    rect->bottom = GetSystemMetrics(SM_CYVIRTUALSCREEN);
}


void
screen_get_screen_rect_from_point(POINT pt, LPRECT rect)
{
    _screen_search_callback_t data = {
        .lp_point = &pt,
        .lp_rect = rect,
    };

    EnumDisplayMonitors(NULL, NULL, _screen_search_proc, (LPARAM)&data);
}


bool
screen_rect_is_within_screen_bounds(LPRECT rect)
{
    _screen_contains_callback_t data = {
        .corners = {
            {.pt = {.x = rect->left,  .y = rect->top},    .is_within = false},
            {.pt = {.x = rect->right, .y = rect->top},    .is_within = false},
            {.pt = {.x = rect->right, .y = rect->bottom}, .is_within = false},
            {.pt = {.x = rect->left,  .y = rect->bottom}, .is_within = false},
        },
    };

    EnumDisplayMonitors(NULL, NULL, _screen_contains_rect_proc, (LPARAM)&data);

    return data.corners[0].is_within && data.corners[1].is_within &&
           data.corners[2].is_within && data.corners[3].is_within;
}


void
screen_trim_rect_to_screen(LPRECT rect)
{
    log_debug("screen_trim_rect_to_screen");

    if (screen_rect_is_within_screen_bounds(rect))
    {
        // No need to trim rect
        log_debug("rect is within screen boundaries");
        return;
    }

    log_debug("rect exceed screen boundaries, trimming...");

    RECT rect_max;
    SetRectEmpty(&rect_max);

    _screen_trim_callback_t data = {
        .lp_rect = rect,
        .lp_rect_max = &rect_max,
        .area = LONG_MIN,
    };

    EnumDisplayMonitors(NULL, NULL, _screen_trim_rect_to_screen_proc,
                        (LPARAM)&data);

    CopyRect(rect, &rect_max);
}