/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <stdbool.h>


bool utils_dwm_is_composition_enabled(void);
uint32_t utils_parse_argb(const char* s);
bool utils_is_hexstring(const char* s);
bool utils_is_windows_11();

#endif /* UTILS_H */