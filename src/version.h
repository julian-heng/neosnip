/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef VERSION_H

#define NEOSNIP_PROG "neosnip"
#define NEOSNIP_VERSION "0.0.0"

#endif /* VERSION_H */