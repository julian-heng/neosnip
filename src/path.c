/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <libgen.h>
#include <string.h>
#include <unistd.h>

#include <windef.h>

#include "path.h"


char*
path_get_filename_without_extension(char* path)
{
    static char buf[MAX_PATH];
    strncpy(buf, basename(path), MAX_PATH);

    char* end = buf + strlen(buf);
    while (end > buf && *end != '.')
    {
        end--;
    }

    if (end > buf)
    {
        *end = '\0';
    }

    return buf;
}


char*
path_get_extension(char* path)
{
    char* ext = strrchr(path, '.');
    return ext != NULL ? ext + 1 : NULL;
}


char*
path_get_cwd(void)
{
    static char buf[MAX_PATH];
    return getcwd(buf, MAX_PATH);
}