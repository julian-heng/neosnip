/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <stdint.h>
#include <stdlib.h>

#include <windows.h>
#include <wingdi.h>

#include <log.c/src/log.h>

#include "bitmap.h"
#include "graphics.h"
#include "image.h"
#include "screen.h"
#include "utils.h"
#include "window.h"

#include "screenshot.h"


static HBITMAP _screenshot_capture_rect(RECT rect);
static void _screenshot_setup_shadow_capture_rect(RECT* capture_rect,
                                                  RECT* window_rect);
static int _screenshot_get_shadow_size();


void
screenshot_spec_init(screenshot_spec_t* screenshot)
{
    screenshot->type = SCREENSHOT_UNKNOWN;
    memset(&(screenshot->spec), 0, sizeof(screenshot->spec));
}


img_t*
screenshot_run(const screenshot_spec_t* screenshot)
{
    _screenshot_spec_t spec = screenshot->spec;

    switch (screenshot->type)
    {
    case SCREENSHOT_FULL:
    case SCREENSHOT_SCREEN:
        return screenshot_capture_rect(spec.capture);

    case SCREENSHOT_WINDOW:
        if (spec.window.transparent)
        {
            return screenshot_capture_window_transparent(&spec);
        }
        else
        {
            return screenshot_capture_window(&spec);
        }

    case SCREENSHOT_UNKNOWN:
    default:
        return NULL;
    }
}


img_t*
screenshot_capture_rect(RECT rect)
{
    log_debug("screenshot_capture_rect");
    log_debug("capturing rect: (%d, %d, %d, %d)",
             rect.left, rect.top, rect.right, rect.bottom);

    HBITMAP h_bitmap = NULL;
    img_t* screenshot = NULL;

    h_bitmap = _screenshot_capture_rect(rect);
    screenshot = bitmap_hbitmap_to_img(h_bitmap);

    DeleteObject(h_bitmap);
    return screenshot;
}


img_t*
screenshot_capture_window(const _screenshot_spec_t* spec)
{
    log_debug("screenshot_capture_window");

    window_t* window = NULL;
    HBITMAP h_bitmap = NULL;
    img_t* screenshot = NULL;

    if (spec == NULL)
    {
        log_error("invalid screenshot spec");
        return NULL;
    }

    window = spec->window.window;
    if (window == NULL)
    {
        log_error("invalid window");
        return NULL;
    }

    window_log(window);

    RECT capture_rect;
    CopyRect(&capture_rect, &(window->rect));

    // Trim the capture rect if it exceeds the screen boundaries
    screen_trim_rect_to_screen(&capture_rect);

    window_bring_to_top(window);
    window_update(window);

    Sleep(10);

    h_bitmap = _screenshot_capture_rect(capture_rect);
    screenshot = bitmap_hbitmap_to_img(h_bitmap);

    DeleteObject(h_bitmap);
    return screenshot;
}


img_t*
screenshot_capture_window_transparent(const _screenshot_spec_t* spec)
{
    log_debug("screenshot_capture_window_transparent");

    // Check if DWM is enabled
    if (!utils_dwm_is_composition_enabled())
    {
        log_debug("dwm is not enabled");
        return screenshot_capture_window(spec);
    }

    if (spec == NULL)
    {
        log_error("invalid screenshot spec");
        return NULL;
    }

    window_t* window = spec->window.window;
    if (window == NULL)
    {
        log_error("invalid window");
        return NULL;
    }

    window_log(window);

    // Enlarge to capture shadow
    RECT capture_rect;
    CopyRect(&capture_rect, &(window->rect));

    if (spec->window.shadow)
    {
        _screenshot_setup_shadow_capture_rect(&capture_rect, &(window->rect));
    }

    // Trim the capture rect if it exceeds the screen boundaries
    screen_trim_rect_to_screen(&capture_rect);

    HWND background = NULL;

    HBRUSH brush_white = (HBRUSH)GetStockObject(WHITE_BRUSH);
    HBRUSH brush_black = (HBRUSH)GetStockObject(BLACK_BRUSH);

    HBITMAP h_white = NULL;
    HBITMAP h_white2 = NULL;
    HBITMAP h_black = NULL;
    HBITMAP h_transparent = NULL;

    img_t* white = NULL;
    img_t* white2 = NULL;
    img_t* black = NULL;
    img_t* transparent = NULL;

    background = graphics_make_rectangle(capture_rect);
    if (background == NULL)
    {
        log_error("failed to make graphics window");
        return NULL;
    }

    window_bring_to_top(window);
    window_update(window);

    Sleep(10);

    log_debug("setting graphics background to white");
    graphics_set_brush(brush_white);
    graphics_refresh(background);
    h_white = _screenshot_capture_rect(capture_rect);

    log_debug("setting graphics background to black");
    graphics_set_brush(brush_black);
    graphics_refresh(background);
    h_black = _screenshot_capture_rect(capture_rect);

    log_debug("setting graphics background to white");
    graphics_set_brush(brush_white);
    graphics_refresh(background);
    h_white2 = _screenshot_capture_rect(capture_rect);

    graphics_close_rectangle(background);

    white = bitmap_hbitmap_to_img(h_white);
    white2 = bitmap_hbitmap_to_img(h_white2);
    black = bitmap_hbitmap_to_img(h_black);

    if (white == NULL || white2 == NULL || black == NULL)
    {
        log_error("failed to capture transparent window");
        goto error_cleanup;
    }

    if (!image_equals(white, white2))
    {
        // White images differ, falling back to using non-transparent screenshot
        log_warn("white images differ, capturing non-transparent screenshot");
        h_transparent = _screenshot_capture_rect(window->rect);
        transparent = bitmap_hbitmap_to_img(h_transparent);
    }
    else
    {
        transparent = image_new_from_img(white);
        image_alpha_blend_inverse(white, black, transparent);
    }

error_cleanup:
    image_free(&white2);
    image_free(&black);
    image_free(&white);

    DeleteObject(h_transparent);
    DeleteObject(h_white2);
    DeleteObject(h_black);
    DeleteObject(h_white);

    return transparent;
}


static HBITMAP
_screenshot_capture_rect(RECT rect)
{
    log_debug("_screenshot_capture_rect");
    log_debug("capturing rect: (%d, %d, %d, %d)",
              rect.left, rect.top, rect.right, rect.bottom);
    int32_t x = rect.left;
    int32_t y = rect.top;
    int32_t w = rect.right - rect.left;
    int32_t h = rect.bottom - rect.top;

    HDC hdc_screen;
    HDC hdc_memory;
    HBITMAP h_bitmap = NULL;
    HGDIOBJ h_old_bitmap;

    hdc_screen = GetDC(NULL);
    hdc_memory = CreateCompatibleDC(hdc_screen);
    h_bitmap = CreateCompatibleBitmap(hdc_screen, w, h);
    h_old_bitmap = SelectObject(hdc_memory, h_bitmap);

    BitBlt(hdc_memory, 0, 0, w, h, hdc_screen, x, y, SRCCOPY | CAPTUREBLT);

    SelectObject(hdc_memory, h_old_bitmap);
    DeleteDC(hdc_memory);
    ReleaseDC(NULL, hdc_screen);

    return h_bitmap;
}


static void
_screenshot_setup_shadow_capture_rect(RECT* capture_rect, RECT* window_rect)
{
    int shadow_size = _screenshot_get_shadow_size();

    if (!InflateRect(capture_rect, shadow_size, shadow_size))
    {
        CopyRect(capture_rect, window_rect);
        capture_rect->top -= shadow_size;
        capture_rect->left -= shadow_size;
        capture_rect->bottom += shadow_size;
        capture_rect->right += shadow_size;
    }

    // Check that the enlarged capture rect does not go beyond the screen
    // boundaries
    if (!screen_rect_is_within_screen_bounds(capture_rect))
    {
        CopyRect(capture_rect, window_rect);
    }
}


static int
_screenshot_get_shadow_size()
{
    int shadow_size = 20;

    if (utils_is_windows_11())
    {
        shadow_size = 80;
    }

    return shadow_size;
}