/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stdint.h>


#define IMAGE_A(p) (((p) & 0xFF000000) >> 24)
#define IMAGE_R(p)  ((p) & 0x000000FF)
#define IMAGE_G(p) (((p) & 0x0000FF00) >> 8)
#define IMAGE_B(p) (((p) & 0x00FF0000) >> 16)

#define IMAGE_ARGB(a, r, g, b) (((a) << 24) | ((b) << 16) | ((g) << 8) | (r))


typedef enum img_ext_t
{
    IMAGE_PNG,
    IMAGE_BMP,
    IMAGE_TGA,
    IMAGE_JPG,
    IMAGE_UNKNOWN
} img_ext_t;


typedef struct img_t
{
    uint32_t width;
    uint32_t height;
    uint32_t* data;
    int jpg_quality;
} img_t;


img_t* image_new(uint32_t width, uint32_t height);
img_t* image_new_from_img(img_t* _img);
void image_free(img_t** img);

void image_blank(img_t* img);
void image_write(img_t* img, char* path, img_ext_t ext);
void image_write_png(img_t* img, char* path);
void image_write_bmp(img_t* img, char* path);
void image_write_tga(img_t* img, char* path);
void image_write_jpg(img_t* img, char* path);
char* image_ext_to_str(img_ext_t ext);
img_ext_t image_str_to_ext(char* str);
bool image_equals(img_t* a, img_t* b);
bool image_validate_path(char* path);
void image_alpha_blend_inverse(img_t* white, img_t* black, img_t* result);
void image_alpha_blend(img_t* img, uint32_t colour);

#endif /* IMAGE_H */