/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <getopt.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include "image.h"
#include "screenshot.h"
#include "utils.h"
#include "version.h"

#include "options.h"


static struct option long_options[] = {
    {"help",        no_argument,        NULL, 'h'},
    {"version",     no_argument,        NULL, 'V'},
    {"full",        no_argument,        NULL, 'f'},
    {"screen",      no_argument,        NULL, 's'},
    {"window",      no_argument,        NULL, 'w'},
    {"transparent", no_argument,        NULL, 't'},
    {"shadow",      no_argument,        NULL, 'd'},
    {"output",      required_argument,  NULL, 'o'},
    {"prompt",      no_argument,        NULL, 'p'},
    {"clipboard",   no_argument,        NULL, 'c'},
    {"background",  required_argument,  NULL, 'b'},
    {"preview",     no_argument,        NULL, 'x'},
    {"verbose",     no_argument,        NULL, 'v'},
    {NULL, 0, NULL, 0},
};


void
options_init(options_t* options)
{
    options->output = NULL;
    options->prompt = false;
    options->clipboard = false;
    options->background = 0xFFFFFFFF;
    options->background_flag = false;
    options->preview = false;
    options->verbose = false;
    screenshot_spec_init(&(options->screenshot));
}


bool
options_parse_command_line_arguments(int argc, char** argv, options_t* options)
{
    char ch;
    char* optstring = "hVfswtdo:pcb:xv";
    while ((ch = getopt_long(argc, argv, optstring, long_options, NULL)) != -1)
    {
        switch (ch)
        {
        case 'h':
            options_print_help();
            return false;

        case 'V':
            options_print_version();
            return false;

        case 'f':
            options->screenshot.type = SCREENSHOT_FULL;
            break;

        case 's':
            options->screenshot.type = SCREENSHOT_SCREEN;
            break;

        case 'w':
            options->screenshot.type = SCREENSHOT_WINDOW;
            break;

        case 't':
            options->screenshot.spec.window.transparent = true;
            break;

        case 'd':
            options->screenshot.spec.window.shadow = true;
            break;

        case 'o':
            if (!image_validate_path(optarg))
            {
                printf("Invalid output path: '%s'\n", optarg);
                return false;
            }

            options->output = optarg;
            break;

        case 'p':
            options->prompt = true;
            break;

        case 'c':
            options->clipboard = true;
            break;

        case 'b':
            options->background = utils_parse_argb(optarg);
            options->background_flag = true;
            break;

        case 'x':
            options->preview = true;
            break;

        case 'v':
            options->verbose = true;
            break;

        case '?':
        default:
            return false;
        }
    }

    return true;
}


void
options_print_help(void)
{
    printf("Usage: neosnip [options]\n"
           "\n"
           "Options:\n"
           "    -h, --help               Display this message\n"
           "    -V, --version            Display the version\n"
           "\n"
           "    -f, --full               Capture full desktop mode\n"
           "    -s, --screen             Capture screen mode\n"
           "    -w, --window             Capture window mode\n"
           "    -t, --transparent        Capture the window with transparency\n"
           "    -d, --shadow             Capture the window shadow\n"
           "    -o, --output <filename>  Save the screenshot to <filename>\n"
           "    -p, --prompt             Prompt the save file dialog\n"
           "    -c, --clipboard          Copy the screenshot to clipboard\n"
           "    -b, --background <color> Set the screenshot background\n"
           "                             to <color> (#[a]rgb or #[aa]rrggbb)\n"
           "    -v, --verbose            Enable verbose printing\n"
           "    -x, --preview            Preview screenshot after capturing\n"
           "\n");
}


void
options_print_version(void)
{
    printf("%s %s\n", NEOSNIP_PROG, NEOSNIP_VERSION);
}