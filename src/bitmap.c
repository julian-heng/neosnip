/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#include <windows.h>
#include <wingdi.h>

#include "image.h"

#include "bitmap.h"


img_t*
bitmap_hbitmap_to_img(HBITMAP h_bitmap)
{
    HDC hdc_screen;
    HDC hdc_memory;

    BITMAP bitmap;
    img_t* img = NULL;

    if (h_bitmap == NULL)
    {
        return NULL;
    }

    hdc_screen = GetDC(NULL);
    hdc_memory = CreateCompatibleDC(hdc_screen);

    GetObject(h_bitmap, sizeof(BITMAP), &bitmap);

    BITMAPINFOHEADER bi = {
        .biSize = sizeof(BITMAPINFOHEADER),
        .biWidth = bitmap.bmWidth,
        .biHeight = -bitmap.bmHeight,
        .biPlanes = 1,
        .biBitCount = 32,
        .biCompression = BI_RGB,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };

    img = image_new(bitmap.bmWidth, bitmap.bmHeight);
    GetDIBits(hdc_memory, h_bitmap, 0, bitmap.bmHeight, img->data,
              (BITMAPINFO*)&bi, DIB_RGB_COLORS);

    // Pixel order is different for stb_image, need to swap blue and red
    size_t size = img->width * img->height;
    for (size_t i = 0; i < size; i++)
    {
        uint32_t pixel = img->data[i];
        uint32_t r = pixel & 0x00FF0000;
        uint32_t g = pixel & 0x0000FF00;
        uint32_t b = pixel & 0x000000FF;

        // Alpha channel is hard set to opaque
        img->data[i] = 0xFF000000 | (b << 16) | g | (r >> 16);
    }

    DeleteDC(hdc_memory);
    ReleaseDC(NULL, hdc_screen);

    return img;
}


HBITMAP
bitmap_img_to_hbitmap(img_t* img)
{
    HBITMAP h_bitmap;

    size_t size = img->width * img->height;
    uint32_t* copy = malloc(size * sizeof(uint32_t));
    memcpy(copy, img->data, size * sizeof(uint32_t));

    // Need to convert the pixel order for a stb_image back to HBITMAP
    for (size_t i = 0; i < size; i++)
    {
        uint32_t pixel = copy[i];
        uint32_t a = IMAGE_A(pixel);
        uint32_t r = IMAGE_R(pixel);
        uint32_t g = IMAGE_G(pixel);
        uint32_t b = IMAGE_B(pixel);

        copy[i] = (a << 24) | (r << 16) | (g << 8) | b;
    }

    h_bitmap = CreateBitmap(img->width, img->height, 1, 32, copy);
    free(copy);

    return h_bitmap;
}