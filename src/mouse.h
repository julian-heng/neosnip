/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright (C) 2022 Julian Heng */

#ifndef MOUSE_H
#define MOUSE_H


void mouse_get_coords_on_click(POINT* point);

#endif /* MOUSE_H */